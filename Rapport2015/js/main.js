$(document).ready(function() {
    var cookieValue = $.cookie("firsttime");
    if(cookieValue == undefined){
        $(".coachlines").show();
        $.cookie("firsttime", false);
    }

    $(".coachlines").click(function(){
        $(this).hide();
    });
//alert($("#hid_lang").val());
    if($("#hid_lang").val() == "en"){
        $strRead = "Read this article";
        $strClose = "Close this article";
    }
    else{
        $strRead = "Lire cet article";
        $strClose = "Fermer cet article";
    }

    $('#modalVideo').on('hidden.bs.modal', function () {
        $("iframe.wistia_embed").attr("src", jQuery("iframe.wistia_embed").attr("src"));
        console.log("CLOSED");
    });

    //autoPlayYouTubeModal("#rail_video");

    //FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
    function autoPlayYouTubeModal(strVideo) {
        var trigger = $(strVideo);
        //var trigger = $("body").find('[data-toggle="modal"]');
        trigger.click(function () {
            var theModal = $(this).data("target"),
            videoSRC = $(this).attr("data-theVideo"),
            videoSRCauto = videoSRC + "?autoplay=1";
            $(theModal + ' iframe').attr('src', videoSRCauto);
            $(theModal + ' button.close').click(function () {
                $(theModal + ' iframe').attr('src', videoSRC);
            });
            $('.modal').click(function () {
                $(theModal + ' iframe').attr('src', videoSRC);
            });
        });
    }


    autoPlayYouTubeModal("#rail_video_fr");

    autoPlayYouTubeModal("#anniversary_video_fr");

    autoPlayYouTubeModal("#president_video_fr");


    $(".scroll").click(function(event) {
        event.preventDefault();
        $('html,body').animate( { scrollTop:$(this.hash).offset().top } , 1000);
    } );

    if (Modernizr.touch) {

    }else{
        $('.bxslider').bxSlider();
    }

    $(function () {
        var currentHash = "#";
        $(document).scroll(function () {
            $('.anchor-offset').each(function () {
                var top = window.pageYOffset;
                var distance = top - $(this).offset().top;
                var hash = $(this).attr('id');
                // 30 is an arbitrary padding choice,
                // if you want a precise check then use distance===0
                if (distance < 20 && distance > -20 && currentHash != hash) {
                    window.location.hash = (hash);
                    currentHash = hash;
                    var previousHash = parseInt(hash.slice(1)) - 1;
                    var nextHash = parseInt(hash.slice(1)) + 1;

                    if (previousHash == 0) {
                        $('#prev-link').attr('href', "#");
                        $('#next-link').attr('href', "#a2");
                    } else {
                        $('#prev-link').attr('href', "#a" + previousHash);
                        $('#next-link').attr('href', "#a" + nextHash);
                    }
                }
            });
        });
    });

    /* Fade out loader */

    setTimeout( function(){
        $(".se-pre-con").fadeOut(1200);
    }  , 2000 );

    //$(window).load(function() {
    //    // Animate loader off screen
    //    $(".se-pre-con").fadeOut(1200);
    //});

/*menu icon animation*/

    document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
        this.classList.toggle( "active" );
    });

    /*opening and closing menu*/

     $("#nav-toggle").click(function(){
      if($("#fold-out-menu").hasClass("hiding")) {
        $("#fold-out-menu").switchClass( "hiding", "showing", 2000, "easeInOut" );
        

      } else { 
        $("#fold-out-menu").switchClass( "showing", "hiding", 2000, "easeInOut" );


      }
    });

      $(document).on('click', function(event) {
            if($("#fold-out-menu").hasClass('showing')) {
                if ($(event.target).closest('.table-contents-link').length){
                    $("#fold-out-menu").switchClass( "showing", "hiding", 2000, "easeInOut" );
                    $("#nav-toggle").removeClass("active");
                            }
                }
        });

      /*opening and closing*/

      




  $('.model1.content-box').readmore({
    collapsedHeight: 400,
    moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">' + $strRead + '</span></a>',
    //moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">Read this article</span></a>',
    lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">' + $strClose + '</span></a>',
    //lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">Close this article</span></a>',
    afterToggle: function(trigger, element, expanded) {

    if(! expanded) { // The "Close" link was clicked
      $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
       
    }
  }
  });

    $('.model2.content-box').readmore({
    collapsedHeight: 400,
    moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">' + $strRead + '</span></a>',
    //moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">Read this article</span></a>',
    lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">' + $strClose + '</span></a>',
    //lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">Close this article</span></a>',
    afterToggle: function(trigger, element, expanded) {
    if(! expanded) { // The "Close" link was clicked
      $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
      
    }
  }
  });

    $('.model3.content-box').readmore({
    collapsedHeight: 320,
    moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">' + $strRead + '</span></a>',
    //moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">Read this article</span></a>',
    lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">' + $strClose + '</span></a>',
    //lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">Close this article</span></a>',
    afterToggle: function(trigger, element, expanded) {
    if(! expanded) { // The "Close" link was clicked
      $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
     
    }
  }
  });

    $('.model4.content-box').readmore({
    collapsedHeight: 320,
    moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">' + $strRead + '</span></a>',
    //moreLink: '<a class="model1 read-more open-btn" href="#"><span class="closed">Read this article</span></a>',
    lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">' + $strClose + '</span></a>',
    //lessLink: '<a class="model1 read-more open-btn" href="#"><span class="opened">Close this article</span></a>',
    afterToggle: function(trigger, element, expanded) {
    if(! expanded) { // The "Close" link was clicked
      $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
      
    }
  }
  });

  
  /*
**** Old system. Was buggy because readmore would reset open-btn on each click.

  $('#section-1').find('.open-btn').on('click', function(){history.pushState(null, null, '#a1');});*/

/* 
**** Echo the IDs in console. Isnt necessary anymore with the scroll

  var updatedurl;
  $( ".anchor-offset" ).each(function( index ) {

      updatedurl = $(this).attr("id");
      console.log(updatedurl);

  });
*/
    


  $(function () {
    $('nav a[href*=#]').on('click', function (event) {
        var $anchor = $(this),
            name = $anchor.attr("href").match(/#(.+)/i)[1];

        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 100
        }, 1000, function () {
            if (history.pushState) {
                history.pushState(null, null, "#" + name);
            } else {
                location.hash = name;
            }
        });
        event.preventDefault();
    });
  });


/* opening and closing of articles 

var slideHeight = 400;

$(".section-class").each(function() {

    var $this = $(this);
    var $wrap = $this.children(".content-box");
    var defHeight = $wrap.height();

    function setHeight(){
	defHeight = $wrap.children('.measurer').height();

	};

	
    
    if (defHeight >= slideHeight) {
    	
    	$(window).resize(setHeight);
    	
        var $readMore = $this.find(".read-more");
        $wrap.css("height", slideHeight + "px");
        $readMore.append("<span class='closed'>Read this article</span>");
        $readMore.bind("click", function(event) {
            var curHeight = $wrap.height();
            if (curHeight == slideHeight) {
                history.pushState({id: 'A1'}, '', '#a1');
                $wrap.animate({
                    height: defHeight
                }, "normal");
                $(this).empty();
                $(this).append("<span class='opened'>Close this article</span>");
                $wrap.siblings(".gradient").fadeOut();
            } else {
                $wrap.animate({
                    height: slideHeight
                }, "normal");
                $(this).empty();
                $(this).append("<span class='closed'>Read this article</span>");
                $wrap.siblings(".gradient").fadeIn();
            }
            return false;
        });

        
    }

    

});
*/

    /*advanced smooth scroll for anchors*/

//  function filterPath(string) {
//  return string
//    .replace(/^\//,'')
//    .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
//    .replace(/\/$/,'');
//  }
//  var locationPath = filterPath(location.pathname);
//  var scrollElem = scrollableElement('html', 'body');
// 
//  $('a[href*=#]').each(function() {
//    var thisPath = filterPath(this.pathname) || locationPath;
//    if (  locationPath == thisPath
//    && (location.hostname == this.hostname || !this.hostname)
//    && this.hash.replace(/#/,'') ) {
//      var $target = $(this.hash), target = this.hash;
//      if (target) {
//        var targetOffset = $target.offset().top;
//        $(this).click(function(event) {
//          event.preventDefault();
//          $(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
//            location.hash = target;
//          });
//        });
//      }
//    }
//  });
// 
//  // use the first element that is "scrollable"
//  function scrollableElement(els) {
//    for (var i = 0, argLength = arguments.length; i <argLength; i++) {
//      var el = arguments[i],
//          $scrollElement = $(el);
//      if ($scrollElement.scrollTop()> 0) {
//        return el;
//      } else {
//        $scrollElement.scrollTop(1);
//        var isScrollable = $scrollElement.scrollTop()> 0;
//        $scrollElement.scrollTop(0);
//        if (isScrollable) {
//          return el;
//        }
//      }
//    }
//    return [];
//  };
 
});





